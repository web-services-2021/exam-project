package models;

import java.util.UUID;

public abstract class Message {
    protected String id = UUID.randomUUID().toString();
    double amount;
    String description;

    public Message() {}

    public Message(double amount, String description) {
        this.amount = amount;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
