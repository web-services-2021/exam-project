package models;

public class AccountValidationResponse extends Message {
    private String customerCpr;
    private String merchantCpr;


    public AccountValidationResponse(String id,String customerCpr, String merchantCpr, double amount, String description) {
        super(amount, description);
        this.customerCpr = customerCpr;
        this.merchantCpr = merchantCpr;
        this.id = id;
    }


    public String getCustomerCpr() {
        return customerCpr;
    }

    public void setCustomerCpr(String customerCpr) {
        this.customerCpr = customerCpr;
    }

    public String getMerchantCpr() {
        return merchantCpr;
    }

    public void setMerchantCpr(String merchantCpr) {
        this.merchantCpr = merchantCpr;
    }
}
