package com.dtu.moneytransfer.BL;

import com.google.gson.Gson;
import dtu.ws.fastmoney.*;
import messaging.Event;
import messaging.EventReceiver;
import messaging.EventSender;
import models.AccountValidationResponse;

import java.math.BigDecimal;
import java.util.ArrayList;

public class TransferService implements EventReceiver {

    public BankService bank;
    public EventSender eventS;
    ArrayList<String> bindingKeys;

    public TransferService(EventSender es) {
        this.bank =  new BankServiceService().getBankServicePort();
        this.eventS = es;
        this.bindingKeys = new ArrayList<>();
        this.bindingKeys.add("transferRequest.request");
    }

    public void transferMoney(String debtor, String creditor, BigDecimal amount, String description) throws BankServiceException_Exception {
        bank.transferMoneyFromTo(debtor, creditor, amount, description);
    }

    public String getAccountIdByCpr(String cpr) throws BankServiceException_Exception {
        return bank.getAccountByCprNumber(cpr).getId();
    }

    private void handleTransferRequest(Event event) throws Exception {
        AccountValidationResponse accountValidationResponseA = new Gson().fromJson((String) event.getArguments()[0], AccountValidationResponse.class);

        try {
            String customerId = getAccountIdByCpr(accountValidationResponseA.getCustomerCpr());
            String merchantId = getAccountIdByCpr(accountValidationResponseA.getMerchantCpr());

            transferMoney(
                customerId,
                merchantId,
                BigDecimal.valueOf(accountValidationResponseA.getAmount()),
                accountValidationResponseA.getDescription()
            );
            eventS.sendEvent("transferRequest.response.success", new Event("transferRequestSuccess", new Object[]{
               accountValidationResponseA.getId()
            }));
        } catch (BankServiceException_Exception ignored) {
            eventS.sendEvent("transferRequest.response.failure", new Event("transferRequestFailure", new Object[]{
                accountValidationResponseA.getId()
            }));
        }
    }

    @Override
    public void receiveEvent(Event event) throws Exception {
        if(event.getEventType().equals("transferRequest")) {
            handleTransferRequest(event);
        }
    }

    @Override
    public ArrayList<String> getBindingKeys() throws Exception {
        if (this.bindingKeys.size() == 0) {
            throw new Exception("No binding keys specified");
        }
        return bindingKeys;
    }
}
