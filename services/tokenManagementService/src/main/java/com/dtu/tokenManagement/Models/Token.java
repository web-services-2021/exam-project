package com.dtu.tokenManagement.Models;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

@Entity
public class Token {
    private String id;
    private String userId;
    private Date expirationDate;
    private boolean used;

    public Token() {

    }

    public Token(String userId) {
        this.id = UUID.randomUUID().toString();
        this.userId = userId;

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, 1);

        this.expirationDate  = cal.getTime();
    }


    public void setId(String id) {
        this.id = id;
    }

    @Id
    public String getId() {
        return id;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }

    public boolean isUsed() {
        return used;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() { return userId; }

    public void setExpirationDate(Date expirationDate) { this.expirationDate = expirationDate; }

    public boolean getIsExpired() {
        Date today = new Date();
        return today.after(expirationDate);
    }

    public boolean validate() {
        return !used && !getIsExpired();
    }
}
