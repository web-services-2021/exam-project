package com.dtu.client.BL;

import com.dtu.client.Models.PaymentRESTRequest;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import dtu.ws.fastmoney.User;
import javax.ws.rs.client.*;
import javax.ws.rs.core.*;

import java.math.BigDecimal;
import java.util.List;

public class DTUPayClient {

    public BankService bank;
    WebTarget accountBaseUrl, paymentBaseUrl;

    public DTUPayClient() {
        Client client = ClientBuilder.newClient();
        accountBaseUrl = client.target("http://localhost:8081/");
        paymentBaseUrl = client.target("http://localhost:8080/");
        bank =  new BankServiceService().getBankServicePort();
    }

    public String createBankAccount(User user, BigDecimal balance) throws BankServiceException_Exception {
        return bank.createAccountWithBalance(user, balance);
    }

    public String createDtuPayAccount(String cpr) throws Exception {
        Form formData = new Form();
        formData.param("cpr", cpr);

        Response response = accountBaseUrl.path("accounts").request().post(Entity.form(formData));
        if (response.getStatus() == 400) {
            throw new Exception(response.readEntity(String.class));
        }
        return response.getHeaderString("location").replace(accountBaseUrl.getUri() + "accounts/", "");
    }

    public List<String> requestTokens(String customerId, int tokensAmount) throws Exception {
        Form formData = new Form();
        formData.param("userId", customerId);
        formData.param("requestedTokens", String.valueOf(tokensAmount));

        Response response = accountBaseUrl.path("tokens").request().post(Entity.form(formData));
        if (response.getStatus() == 400) {
            throw new Exception(response.readEntity(String.class));
        }
        return response.readEntity(new GenericType<>() {});
    }

    public boolean makePayment(PaymentRESTRequest payment) throws Exception {
        Response response = paymentBaseUrl.path("payments").request()
                .post(Entity.entity(payment, MediaType.APPLICATION_JSON));
        if (response.getStatus() == 400) {
            throw new Exception(response.readEntity(String.class));
        }
        return response.getStatus() == 200;
    }

}
