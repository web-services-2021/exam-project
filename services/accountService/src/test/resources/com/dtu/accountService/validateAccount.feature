Feature: Validate account
  Scenario: Valid account
    Given An event to validate account with cid "cid" and mid "mid"
    And An account with id "cid" and cpr "001122-3344" exists
    And An account with id "mid" and cpr "001122-3345" exists
    When I validate the account
    Then I send event with type "accountValidationSuccess"

  Scenario: Invalid account no customer
    Given An event to validate account with cid "cid" and mid "mid"
    And An account with id "mid" and cpr "001122-3345" exists
    When I validate the account
    Then I send event with type "accountValidationFailure"

  Scenario: Invalid account no merchant
    Given An event to validate account with cid "cid" and mid "mid"
    And An account with id "cid" and cpr "001122-3345" exists
    When I validate the account
    Then I send event with type "accountValidationFailure"