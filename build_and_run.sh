#!/bin/bash
set -e

# Build and install the libraries
# abstracting away from using the
# RabbitMq message queue
pushd libraries/messaging-utilities
chmod +x ./build.sh
./build.sh
popd 

# Build the services
pushd services/paymentService
chmod +x ./build.sh
./build.sh
popd 

# Build the services
pushd services/tokenManagementService
chmod +x ./build.sh
./build.sh
popd

pushd services/moneyTransferService
chmod +x ./build.sh
./build.sh
popd

pushd services/accountService
chmod +x ./build.sh
./build.sh
popd

pushd end_to_end_tests
chmod +x ./deploy.sh
./deploy.sh 
sleep 15s
chmod +x test.sh
./test.sh
popd

# Cleanup the build images
docker image prune -f
