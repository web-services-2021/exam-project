package com.dtu.accountService.Exceptions;

public class TokenRequestException extends Exception{

    public TokenRequestException(String message) {
        super(message);
    }

    public TokenRequestException() {
        super();
    }
}
