package com.dtu.moneytransfer;

import messaging.EventSender;
import messaging.rabbitmq.RabbitMqListener;
import messaging.rabbitmq.RabbitMqSender;

import com.dtu.moneytransfer.BL.TransferService;

public class StartUp {

    public static void main(String[] args) throws Exception {
        EventSender s = new RabbitMqSender();
        TransferService service = new TransferService(s);
        new RabbitMqListener(service).listen();
    }
}

