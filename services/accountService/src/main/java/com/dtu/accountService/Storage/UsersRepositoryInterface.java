package com.dtu.accountService.Storage;

import com.dtu.accountService.Models.User;
import com.dtu.accountService.Exceptions.UserAlreadyExistsException;
import com.dtu.accountService.Exceptions.UserDoesNotExistException;

public interface UsersRepositoryInterface {
    String getCprByUserId(String userId) throws UserDoesNotExistException;

    void addUser(User user) throws UserAlreadyExistsException;
}
