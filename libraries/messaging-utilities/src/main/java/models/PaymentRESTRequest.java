package models;

public class PaymentRESTRequest {
    private String tokenId;
    private String merchantId;
    private double amount;
    private String description;

    public PaymentRESTRequest() {}

    public PaymentRESTRequest(String tokenId, String merchantId, double amount, String description) {
        this.tokenId = tokenId;
        this.merchantId = merchantId;
        this.amount = amount;
        this.description = description;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }
}
