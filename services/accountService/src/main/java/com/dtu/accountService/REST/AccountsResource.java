package com.dtu.accountService.REST;

import com.dtu.accountService.BL.AccountService;
import com.dtu.accountService.BL.AccountServiceFactory;
import com.dtu.accountService.Exceptions.UserAlreadyExistsException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;


@Path("/accounts")
public class AccountsResource {

    @POST
    @Produces(MediaType.TEXT_PLAIN)
    public Response registerAccount(@FormParam("cpr") String cpr) {
        try{
            AccountService service = AccountServiceFactory.getAccountService();
            return Response.created(URI.create("accounts/" + service.registerAccount(cpr))).build();
        } catch (UserAlreadyExistsException e) {
            return Response.status(400).entity(e.getMessage()).build();
        }
    }
}