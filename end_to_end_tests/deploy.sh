#!/bin/bash
set -e

docker-compose stop
docker-compose rm -f payment_service token_service moneytransfer_service account_service
docker image prune -f
docker-compose up -d rabbitMq
sleep 10s
docker-compose up -d payment_service token_service moneytransfer_service account_service

