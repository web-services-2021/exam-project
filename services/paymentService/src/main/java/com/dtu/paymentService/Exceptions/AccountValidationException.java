package com.dtu.paymentService.Exceptions;

public class AccountValidationException extends Exception {
    public AccountValidationException(String message) {
        super(message);
    }
}
