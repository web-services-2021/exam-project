package com.dtu.accountService;

import com.dtu.accountService.BL.AccountService;
import com.dtu.accountService.Models.User;
import com.dtu.accountService.Exceptions.UserAlreadyExistsException;
import com.dtu.accountService.Storage.InMemoryUsersRepository;
import com.google.gson.Gson;
import io.cucumber.java.en.*;
import messaging.Event;
import models.TokenValidationResponse;
import org.junit.Assert;

public class ValidateAccountStepDefs {
    AccountService service;
    InMemoryUsersRepository repo;
    Event e;
    TokenValidationResponse request;

    public ValidateAccountStepDefs() {
        repo =  new InMemoryUsersRepository();
        service = new AccountService((routingKey, event) -> e = event, repo);
    }

    @Given("An event to validate account with cid {string} and mid {string}")
    public void aRequestToValidateAccountWithId(String arg0, String arg1) {
        request = new TokenValidationResponse("unique",arg0, arg1,2,"desc");

    }

    @And("An account with id {string} and cpr {string} exists")
    public void anAccountWithIdAndCprExists(String arg0, String arg1) throws UserAlreadyExistsException {
        User user = new User(arg1);
        user.setId(arg0);
        repo.addUser(user);
    }

    @When("I validate the account")
    public void iValidateTheAccount() throws Exception{
        Event ev = new Event("accountValidation", new Object[]{request});
        ev.getArguments()[0] = new Gson().toJson(ev.getArguments()[0]);
        service.receiveEvent(ev);
    }

    @Then("I send event with type {string}")
    public void iSendEventWithType(String arg0) {
        Assert.assertEquals(arg0, e.getEventType());
    }
}
