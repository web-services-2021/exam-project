package com.dtu.paymentService;

import com.dtu.paymentService.BL.PaymentService;
import com.google.gson.Gson;
import io.cucumber.java.en.*;
import messaging.Event;
import models.AccountValidationResponse;
import models.TokenValidationRequest;
import models.TokenValidationResponse;
import org.junit.Assert;

public class PaymentStepdefs {
    PaymentService pr;
    TokenValidationRequest payment;
    Event event;

    public PaymentStepdefs() {
        pr = new PaymentService((routingKey, ev) -> event = ev);
    }

    @When("a payment request is received with token {string}, merchantId {string}, amount {int} and description {string}")
    public void aPaymentRequestIsReceivedWithTokenMerchantIdAmountAndDescription(String arg0, String arg1, int arg2, String arg3) {
        payment = new TokenValidationRequest(arg0, arg1, arg2, arg3);
        pr.validateToken(payment);
    }

    @Then("I have sent event {string}")
    public void iHaveSentEvent(String arg0) {
        //Assert.assertEquals(arg0, event.getEventType());
    }

    @Then("I have not sent an event")
    public void iHaveNotSentAnEvent() {
        Assert.assertNull(event);
    }

    @When("a token validation success response is received")
    public void aTokenValidationSuccessResponseIsReceived() {
        pr.receiveEvent(new Event("tokenValidationSuccess", new Object[]{
                new Gson().toJson(new TokenValidationResponse(null, "cid", "mid", 10, "Hello"))
        }));
    }

    @When("a token validation failure response is received")
    public void aTokenValidationFailureResponseIsReceived() {
        pr.receiveEvent(new Event("tokenValidationFailure", new Object[]{
                new Gson().toJson(new TokenValidationResponse(null, "cid", "mid", 10, "Hello"))
        }));
    }

    @When("an account validation success response is received")
    public void anAccountValidationSuccessResponseIsReceived() {
        pr.receiveEvent(new Event("accountValidationSuccess", new Object[]{
                new Gson().toJson(new AccountValidationResponse(null,"cidcpr", "midcpr", 10, "Hello"))
        }));
    }

    @When("an account validation failure response is received")
    public void anAccountValidationFailureResponseIsReceived() {
        pr.receiveEvent(new Event("accountValidationFailure", new Object[]{
                new Gson().toJson(new TokenValidationResponse(null, "cidcpr", "midcpr", 10, "Hello"))
        }));
    }
}
