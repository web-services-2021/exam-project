package com.dtu.paymentService.Exceptions;

public class TokenValidationFailedException extends Exception {

    public TokenValidationFailedException(String invalidToken) {
        super(invalidToken);
    }
}
