package com.dtu.client;

import com.dtu.client.BL.DTUPayClient;
import com.dtu.client.Models.PaymentRESTRequest;
import dtu.ws.fastmoney.BankServiceException_Exception;
import io.cucumber.java.After;
import io.cucumber.java.en.*;
import org.junit.Assert;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class EndToEndStepsDef {

    private final DTUPayClient dtuPayClient = new DTUPayClient();
    private final Map<Integer, Customer> customers = new HashMap<>();
    private Merchant merchant;
    private Exception exception;
    private String lastUsedToken;

    @Given("customer {int} with name {string} {string} and CPR {string} has a bank account")
    public void customerWithNameAndCPRHasABankAccount(int arg0, String arg1, String arg2, String arg3) {
        Customer customer = new Customer(arg1, arg2, arg3);
        customers.put(arg0, customer);
    }

    @And("the balance of customer {int} at the bank is {int} kr")
    public void theBalanceOfCustomerAtTheBankIsKr(int arg0, int arg1) {
        try {
            Customer customer = customers.get(arg0);
            customer.bankId = dtuPayClient.createBankAccount(customer.bankAccount, BigDecimal.valueOf(arg1));
            customers.replace(arg0, customer);
        } catch (BankServiceException_Exception e) {
            exception = e;
        }
    }

    @And("customer {int} is registered with DTUPay")
    public void customerIsRegisteredWithDTUPay(int arg0) {
        try {
            Customer customer = customers.get(arg0);
            customer.dtuPayId = dtuPayClient.createDtuPayAccount(customer.cpr);
            customers.replace(arg0, customer);
        } catch (Exception e) {
            exception = e;
        }
    }

    @And("customer {int} has requested {int} tokens")
    public void customerHasRequestedTokens(int arg0, int arg1) {
        try {
            Customer customer = customers.get(arg0);
            customer.tokens = dtuPayClient.requestTokens(customer.dtuPayId, arg1);
            customers.replace(arg0, customer);
        } catch (Exception e) {
            exception = e;
        }
    }

    @And("the merchant with name {string} {string} and CPR {string} has a bank account")
    public void theMerchantWithNameAndCPRHasABankAccount(String arg0, String arg1, String arg2) {
        merchant = new Merchant(arg0, arg1, arg2);
    }

    @And("the balance of the merchant at the bank is {int} kr")
    public void theBalanceOfTheMerchantAtTheBankIs(int arg0) {
        try {
            merchant.bankId = dtuPayClient.createBankAccount(merchant.bankAccount, BigDecimal.valueOf(arg0));
        } catch (BankServiceException_Exception e) {
            exception = e;
        }
    }

    @And("the merchant is registered with DTUPay")
    public void theMerchantIsRegisteredWithDTUPay() {
        try {
            merchant.dtuPayId = dtuPayClient.createDtuPayAccount(merchant.cpr);
        } catch (Exception e) {
            exception = e;
        }
    }

    @When("the merchant initiates a payment for {int} kr by the customer {int}")
    public void theMerchantInitiatesAPaymentForKrByTheCustomer(int arg0, int arg1) {
        Customer customer = customers.get(arg1);
        lastUsedToken = customer.tokens.remove(0);

        PaymentRESTRequest paymentRequest = new PaymentRESTRequest(
                lastUsedToken, merchant.dtuPayId, arg0, "Test");
        try {
            customer.addPayment(dtuPayClient.makePayment(paymentRequest));
        } catch (Exception e) {
            exception = e;
        }
    }

    @Then("the payment {int} from customer {int} is successful")
    public void thePaymentFromCustomerIsSuccessful(int arg0, int arg1) {
        Assert.assertTrue(customers.get(arg1).paymentStatuses.get(arg0 - 1));
    }

    @And("the balance of customer {int} at the bank will be {int} kr")
    public void theBalanceOfCustomerAtTheBankWillBeKr(int arg0, int arg1) {
        try {
            Assert.assertEquals(arg1, dtuPayClient.bank.getAccount(customers.get(arg0).bankId).getBalance().intValue());
        } catch (BankServiceException_Exception e) {
            exception = e;
        }
    }

    @And("the balance of the merchant at the bank will be {int} kr")
    public void theBalanceOfTheMerchantAtTheBankWillBeKr(int arg0) {
        try {
            Assert.assertEquals(arg0, dtuPayClient.bank.getAccount(merchant.bankId).getBalance().intValue());
        } catch (BankServiceException_Exception e) {
            exception = e;
        }
    }

    @And("the customer {int} uses the same token as in payment {int}")
    public void theCustomerUsesTheSameTokenAsInPayment(int arg0, int arg1) {
        Customer customer = customers.get(arg0);

        PaymentRESTRequest paymentRequest = new PaymentRESTRequest(
                lastUsedToken, merchant.dtuPayId, arg1 - 1, "Test");
        try {
            customer.addPayment(dtuPayClient.makePayment(paymentRequest));
        } catch (Exception e) {
            exception = e;
        }
    }

    @Then("a {string} error message is received")
    public void aErrorMessageIsReceived(String arg0) {
        Assert.assertEquals(arg0, exception.getMessage());
    }

    @After
    public void cleanUp() throws BankServiceException_Exception {
        for (Customer customer : customers.values()) {
            if (customer.bankId != null) {
                dtuPayClient.bank.retireAccount(customer.bankId);
                System.out.printf("Customer %s bank account removed%n", customer.cpr);
            }
        }

        if (merchant != null && merchant.bankId != null) {
            dtuPayClient.bank.retireAccount(merchant.bankId);
            System.out.printf("Merchant %s bank account removed%n", merchant.cpr);
        }
    }
}
