package messaging;

public interface EventSender {

	void sendEvent(String routingKey, Event event) throws Exception;

}
