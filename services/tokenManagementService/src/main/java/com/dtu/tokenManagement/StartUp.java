package com.dtu.tokenManagement;

import com.dtu.tokenManagement.BL.TokenService;
import com.dtu.tokenManagement.BL.TokenServiceFactory;
import messaging.EventSender;
import messaging.rabbitmq.RabbitMqListener;
import messaging.rabbitmq.RabbitMqSender;

public class StartUp {

    public static void main(String[] args) throws Exception {
        EventSender s = new RabbitMqSender();
        TokenService service = new TokenServiceFactory().getTokenService(s);
        new RabbitMqListener(service).listen();
    }
}

