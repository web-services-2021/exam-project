package com.dtu.moneytransfer;

import com.dtu.moneytransfer.BL.TransferService;
import com.google.gson.Gson;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.User;
import io.cucumber.java.After;
import io.cucumber.java.en.*;
import messaging.Event;
import models.AccountValidationResponse;
import org.junit.Assert;

import java.math.BigDecimal;

public class TransferStepdefs {

    private TransferService transferService;
    private String customerId,merchantId;
    private User customer, merchant;
    private int transferAmount;
    private Exception exception;
    private Event event;


    public TransferStepdefs() {
        transferService = new TransferService((routingKey, ev) -> event = ev);
    }

    @Given("the customer {string} {string} with CPR {string} has a bank account")
    public void theCustomerWithCPRHasABankAccount(String arg0, String arg1, String arg2) {
        customer = new User();
        customer.setFirstName(arg0);
        customer.setLastName(arg1);
        customer.setCprNumber(arg2);
    }

    @Given("the balance of the customer's account is {int}")
    public void theBalanceOfThatAccountIs(int arg0) {
        customerId = createAccountAndGetAccountId(customer, new BigDecimal(arg0));
    }

    @Given("the merchant {string} {string} with CPR number {string} has a bank account")
    public void theMerchantWithCPRNumberHasABankAccount(String arg0, String arg1, String arg2) {
        merchant = new User();
        merchant.setFirstName(arg0);
        merchant.setLastName(arg1);
        merchant.setCprNumber(arg2);
    }

    @Given("the balance of the merchant's account is {int}")
    public void merchantBalance(int arg0){
        merchantId = createAccountAndGetAccountId(merchant, new BigDecimal(arg0));
    }

    @Given("a customer with CPR {string} does not have a bank account")
    public void aUserWithCPRDoesNotHaveABankAccount(String arg0) {
        customer = new User();
        customer.setCprNumber(arg0);
    }

    @When("the merchant initiates a payment for {int} kr by the customer")
    public void theMerchantInitiatesAPaymentForKrByTheCustomer(int arg0) {
        transferAmount = arg0;
    }

    @When("trying to retrieve the users bank account")
    public void tryingToRetrieveTheUserSBankAccount() {
        try {
            transferService.getAccountIdByCpr(customer.getCprNumber());
        } catch (BankServiceException_Exception e) {
            exception = e;
        }
    }

    @When("I receive event {string}")
    public void iReceiveEvent(String arg0) {
        try {
            transferService.receiveEvent(new Event(arg0, new Object[]{
                    new Gson().toJson(new AccountValidationResponse(
                            "MoneyTranserTest",
                            customer.getCprNumber(),
                            merchant.getCprNumber(),
                            transferAmount,
                            "description"))
            }));
        } catch (BankServiceException_Exception e) {

        } catch (Exception e) {

        }
    }

    @Then("the bank returns an error")
    public void theBankReturnsAnError() {
        //Assert.assertThrows(BankServiceException_Exception.class, () -> { throw exception; } );
        Assert.assertNotNull(exception);
    }

    @Then("I have sent event {string}")
    public void iHaveSentEvent(String arg0) {
        Assert.assertEquals(arg0, event.getEventType());
    }

    @Then("the balance of the customer at the bank is {int} kr")
    public void theBalanceOfTheCustomerAtTheBankIsKr(int arg0) {
        try {
            Assert.assertEquals(
                    arg0,
                    this.transferService.bank.getAccount(customerId).getBalance().intValue()
            );
        } catch (BankServiceException_Exception e) {

        }
    }

    @Then("the balance of the merchant at the bank is {int} kr")
    public void theBalanceOfTheMerchantAtTheBankIsKr(int arg0) {
        try {
            Assert.assertEquals(
                    arg0,
                    this.transferService.bank.getAccount(merchantId).getBalance().intValue()
            );
        } catch (BankServiceException_Exception e) {
        }
    }

    @After
    public void cleanUp() {
        try {
            if (customerId != null) {
                this.transferService.bank.retireAccount(customerId);
            }
            if (merchantId != null) {
                this.transferService.bank.retireAccount(merchantId);
            }
        } catch (BankServiceException_Exception e) {
        }
    }

    //Helper functions
    public String createAccountAndGetAccountId(User user, BigDecimal balance) {
        try {
            return this.transferService.bank.createAccountWithBalance(user, balance);
        } catch (BankServiceException_Exception e) {
            return null;
        }
    }
}
