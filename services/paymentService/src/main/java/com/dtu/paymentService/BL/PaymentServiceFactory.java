package com.dtu.paymentService.BL;

import messaging.EventSender;
import messaging.rabbitmq.RabbitMqListener;
import messaging.rabbitmq.RabbitMqSender;

public class PaymentServiceFactory {
    static PaymentService paymentService = null;

    public static PaymentService getPaymentService() {
        if (paymentService != null) {
            return paymentService;
        }

        EventSender eventSender = new RabbitMqSender();
        paymentService = new PaymentService(eventSender);
        RabbitMqListener rabbitMqListener = new RabbitMqListener(paymentService);
        try {
            rabbitMqListener.listen();
        } catch (Exception e) {
            throw new Error(e);
        }
        return paymentService;
    }

}
