package com.dtu.accountService.Models;

import java.util.UUID;

public class User {
    private String id;
    private String cpr;

    public User(String cpr) {
        this.id = UUID.randomUUID().toString();
        this.cpr = cpr;
    }

    public String getId() {
        return id;
    }

    public String getCpr() {
        return cpr;
    }

    public void setId(String id){this.id = id;}
}
