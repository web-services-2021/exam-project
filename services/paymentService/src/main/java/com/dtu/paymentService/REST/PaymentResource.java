package com.dtu.paymentService.REST;

import com.dtu.paymentService.BL.PaymentServiceFactory;
import com.dtu.paymentService.Exceptions.AccountValidationException;
import com.dtu.paymentService.Exceptions.MoneyTransferFailureException;
import com.dtu.paymentService.Exceptions.TokenValidationFailedException;
import models.PaymentRESTRequest;
import models.TokenValidationRequest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/payments")
public class PaymentResource {
    com.dtu.paymentService.BL.PaymentService paymentService = PaymentServiceFactory.getPaymentService();

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response makePayment(PaymentRESTRequest paymentRequest) throws Exception {
        try {
            paymentService.makePayment(new TokenValidationRequest(paymentRequest));
        } catch (TokenValidationFailedException | AccountValidationException | MoneyTransferFailureException e) {
            return Response.status(400).entity(e.getMessage()).build();
        }

        return Response.ok().build();
    }
}
