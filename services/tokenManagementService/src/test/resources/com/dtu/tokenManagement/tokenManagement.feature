Feature: TokenManagement

  Scenario: User with no tokens requests tokens and success
    Given a user with id "something"
    And user has 0 tokens
    When user requests 1 tokens
    And I receive event "requestTokens"
    Then I have sent event "requestTokensSuccess"
    And user now has 1 tokens

  Scenario: User cannot request more tokens than the limit of 5 tokens
    Given a user with id "something"
    And user has 2 tokens
    When user requests 3 tokens
    And I receive event "requestTokens"
    Then I have sent event "requestTokensFailure"
    And user now has 2 tokens

  Scenario: User requests tokens when has 1 unused token and success
    Given a user with id "something"
    And user has 1 tokens
    When user requests 6 tokens
    And I receive event "requestTokens"
    Then I have sent event "requestTokensFailure"
    And user now has 1 tokens

  Scenario: Validation of Token usage success
    Given a validation request with token id "xxx"
    And token is not expired
    And token is not used
    When I receive event "tokenValidation"
    Then I have sent event "tokenValidationSuccess"

  Scenario: Validation of token usage failure
    Given a validation request with token id "xxx"
    And token is expired
    And token is not used
    When I receive event "tokenValidation"
    Then I have sent event "tokenValidationFailure"

  Scenario: Validation of token usage failure
    Given a validation request with token id "xxx"
    And token is not expired
    And token is used
    When I receive event "tokenValidation"
    Then I have sent event "tokenValidationFailure"

