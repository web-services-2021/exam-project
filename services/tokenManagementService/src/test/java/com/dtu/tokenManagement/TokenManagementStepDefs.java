package com.dtu.tokenManagement;

import com.dtu.tokenManagement.BL.TokenService;
import com.dtu.tokenManagement.Models.Token;
import com.dtu.tokenManagement.Storage.InMemoryTokenRepository;
import com.google.gson.Gson;
import io.cucumber.java.en.*;
import messaging.Event;
import messaging.EventSender;
import models.TokenValidationRequest;
import models.TokenRequestRequest;
import org.junit.Assert;

import java.util.Calendar;
import java.util.UUID;

public class TokenManagementStepDefs {
    private InMemoryTokenRepository tokenRepository;
    private TokenService tokenService;
    private String userId;
    private Exception exception;
    private Event event;
    private Token token;
    private int requestedTokenAmount;
    private boolean isValidated;

    public TokenManagementStepDefs() {
        tokenRepository=  new InMemoryTokenRepository();
        tokenService = new TokenService(tokenRepository, new EventSender() {
            @Override
            public void sendEvent(String routingKey, Event ev) throws Exception {
                event = ev;
            }
        });
    }

    @Given("a user with id {string}")
    public void aUserWithId(String arg0) {
        userId = arg0;
    }

    @Given("user has {int} tokens")
    public void userHasTokens(int arg0) {
        for(int i = 0; i < arg0; i++) {
            tokenService.createToken(userId);
        }
    }

    @When("user requests {int} tokens")
    public void userRequestsTokens(int arg0) {
        requestedTokenAmount = arg0;
    }

    @Then("user now has {int} tokens")
    public void userNowHasTokens(int arg0) {
        int tokensNumber = tokenService.getListOfTokensByUserId(userId).size();
        Assert.assertEquals(arg0, tokensNumber);
    }

    @Given("a validation request with token id {string}")
    public void aValidationRequestWithTokenId(String arg0) {
        this.token = new Token(UUID.randomUUID().toString());
        token.setId(arg0);
        tokenRepository.addToken(token);
    }

    @And("token is not expired")
    public void tokenIsNotExpired() {
        Assert.assertFalse(token.getIsExpired());
    }

    @And("token is not used")
    public void tokenIsNotUsed() {
        Assert.assertFalse(token.isUsed());
    }

    @And("token is expired")
    public void tokenIsExpired() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, -1);
        token.setExpirationDate(cal.getTime());

        Assert.assertTrue(token.getIsExpired());
    }

    @And("token is used")
    public void tokenIsUsed() {
        token.setUsed(true);
        Assert.assertTrue(token.isUsed());
    }

    @When("I receive event {string}")
    public void iReceiveEvent(String arg0) {
        try {
            if (arg0.equals("tokenValidation")) {
                tokenService.receiveEvent(new Event(arg0, new Object[]{
                        new Gson().toJson(new TokenValidationRequest(token.getId(), "mid", 10, "Hello"))
                }));
            } else if (arg0.equals("requestTokens")) {
                tokenService.receiveEvent(new Event(arg0, new Object[]{
                        new Gson().toJson(new TokenRequestRequest(userId, requestedTokenAmount))
                }));
            }
        } catch (Exception e) {
            exception = e;
        }
    }

    @Then("I have sent event {string}")
    public void iHaveSentEvent(String arg0) {
        Assert.assertEquals(arg0, event.getEventType());
    }
}
