package com.dtu.accountService;

import com.dtu.accountService.BL.AccountService;
import com.dtu.accountService.Models.User;
import com.dtu.accountService.Exceptions.UserAlreadyExistsException;
import com.dtu.accountService.Storage.InMemoryUsersRepository;
import io.cucumber.java.en.*;
import org.junit.Assert;

public class CreateAccountStepDefs {
    InMemoryUsersRepository repo;
    AccountService service;
    String cpr;
    UserAlreadyExistsException e;

    public CreateAccountStepDefs() {
        repo =  new InMemoryUsersRepository();
        service = new AccountService(null, repo);
    }

    @Given("a cpr {string}")
    public void aCpr(String arg0) {
        cpr = arg0;
    }

    @And("an account with cpr {string} exists")
    public void anAccountWithCprExists(String arg0) throws UserAlreadyExistsException {
        repo.addUser(new User(arg0));
    }

    @When("I register an account")
    public void iRegisterAnAccount(){
        try {
            service.registerAccount(cpr);
        } catch (UserAlreadyExistsException userAlreadyExistsException) {
            e = userAlreadyExistsException;
        }
    }

    @Then("The account is registered")
    public void theAccountIsRegistered() {
        boolean exists = false;
        for (var u : repo.users){
            if(u.getCpr().equals(cpr)){
                exists = true;
            }
        }
        Assert.assertTrue(exists);
    }

    @Then("An exception is thrown")
    public void anExceptionIsThrown() {
        Assert.assertNotNull(e);
    }

    @And("The account is not registered twice")
    public void theAccountIsNotRegisteredTwice() {
        int amount = 0;
        for (var u : repo.users){
            if(u.getCpr().equals(cpr)){
                amount++;
            }
        }
        Assert.assertEquals(1,amount);
    }
}
