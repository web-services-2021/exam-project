Feature: Payment

  Scenario:
    When a payment request is received with token "token", merchantId "merchantId", amount 100 and description ""
    Then I have sent event "tokenValidation"

  Scenario: 
    When a token validation success response is received
    Then I have sent event "accountValidation"

  Scenario:
    When a token validation failure response is received
    Then I have not sent an event

  Scenario:
    When an account validation success response is received
    Then I have sent event "transferRequest"

  Scenario:
    When an account validation failure response is received
    Then I have not sent an event


