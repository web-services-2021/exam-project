package com.dtu.tokenManagement.BL;

import com.dtu.tokenManagement.Models.*;
import com.dtu.tokenManagement.Storage.TokenRepositoryInterface;
import com.google.gson.Gson;
import messaging.Event;
import messaging.EventReceiver;
import messaging.EventSender;
import models.TokenValidationRequest;
import models.TokenValidationResponse;
import models.TokenRequestRequest;
import models.TokenRequestResponse;

import java.util.ArrayList;


public class TokenService implements EventReceiver {

    TokenRepositoryInterface tokenRepository;
    EventSender eventSender;
    ArrayList<String> bindingKeys;

    public TokenService(TokenRepositoryInterface tokenRepository, EventSender sender) {
        this.tokenRepository = tokenRepository;
        this.eventSender = sender;
        this.bindingKeys = new ArrayList<>();
        this.bindingKeys.add("tokenRequest.request");
        this.bindingKeys.add("tokenValidation.request");
    }

    private void checkIfAbleToGetTokens(int amount, String userId) throws Exception {
        if(amount > 5) {
            throw new Exception("Can not request more than 5 tokens");
        }

        ArrayList<Token> unusedTokens = new ArrayList<>();
        for(Token token : getListOfTokensByUserId(userId)) {
            if(token.validate()) {
                unusedTokens.add(token);
            }
        }

        if(unusedTokens.size() > 1) {
            throw new Exception("Can not request tokens if user has more than 1 token");
        }
    }

    public ArrayList<Token> requestTokens(int amount, String userId) throws Exception {
        checkIfAbleToGetTokens(amount, userId);

        for(int i = 0; i < amount; i++) {
            createToken(userId);
        }

        return getListOfValidTokensByUserId(userId);
    }

    public String createToken(String userId) {
        Token token = new Token(userId);
        tokenRepository.addToken(token);

        return token.getId();
    }

    public ArrayList<Token> getListOfTokensByUserId(String userId) {
        return tokenRepository.getTokensByUserId(userId);
    }

    public ArrayList<Token> getListOfValidTokensByUserId(String userId) {
        ArrayList<Token> tokens = tokenRepository.getTokensByUserId(userId);
        ArrayList<Token> validTokens = new ArrayList<>();

        for(Token token : tokens) {
            if(token.validate()) {
                validTokens.add(token);
            }
        }

        return validTokens;
    }

    private void handleValidation(Event ev) throws Exception {
        TokenValidationRequest pr = new Gson().fromJson((String) ev.getArguments()[0] , TokenValidationRequest.class);
        Token token = tokenRepository.getTokenById(pr.getTokenId());

        if(token != null && token.validate()) {
            eventSender.sendEvent("tokenValidation.response.success", new Event("tokenValidationSuccess", new Object[]{
                new TokenValidationResponse(pr.getId(), token.getUserId(),pr.getMerchantId(),pr.getAmount() , pr.getDescription())
            }));

            token.setUsed(true);
            return;
        }

        eventSender.sendEvent("tokenValidation.response.failure", new Event("tokenValidationFailure", new Object[]{
            new TokenValidationResponse(pr.getId(), null, pr.getMerchantId(),pr.getAmount() , pr.getDescription())
        }));
    }

    private void handleRequestTokens(Event event) throws Exception {
        TokenRequestRequest tokenRequest = new Gson().fromJson((String) event.getArguments()[0], TokenRequestRequest.class);
        int amount = tokenRequest.getNumberOfTokensRequested();
        String userId =  tokenRequest.getUserId();

        try {
            ArrayList<Token> tokens = requestTokens(amount, userId);
            ArrayList<String> tokenIds = new ArrayList<>();

            for(Token token : tokens) {
                tokenIds.add(token.getId());
            }

            eventSender.sendEvent("tokenRequest.response.success", new Event("requestTokensSuccess", new Object[]{
                new TokenRequestResponse(tokenRequest.getId(), tokenRequest.getUserId(), tokenIds)
            }));
        } catch (Exception e) {
            eventSender.sendEvent("tokenRequest.response.failure", new Event("requestTokensFailure", new Object[]{
                new TokenRequestResponse(tokenRequest.getId(), tokenRequest.getUserId(), null)
            }));
        }
    }

    @Override
    public void receiveEvent(Event event) throws Exception {
        if (event.getEventType().equals("tokenValidation")) {
            handleValidation(event);
        } else if(event.getEventType().equals("requestTokens")) {
            handleRequestTokens(event);
        }
    }

    @Override
    public ArrayList<String> getBindingKeys() throws Exception {
        if (this.bindingKeys.size() == 0) {
            throw new Exception("No binding keys specified");
        }
        return bindingKeys;
    }
}