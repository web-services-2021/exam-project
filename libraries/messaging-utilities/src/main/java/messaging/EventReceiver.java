package messaging;

import java.util.ArrayList;

public interface EventReceiver {
	void receiveEvent(Event event) throws Exception;

	ArrayList<String> getBindingKeys() throws Exception;
}
