#!/bin/bash
set -e
mvn clean package
docker-compose build moneytransfer_service