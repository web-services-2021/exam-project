package models;

public class TokenValidationRequest extends Message {
    String tokenId;
    String merchantId;

    public TokenValidationRequest() {

    }

    public TokenValidationRequest(String tokenId, String merchantId, double amount, String description ) {
        super(amount, description);
        this.tokenId = tokenId;
        this.merchantId = merchantId;
    }

    public TokenValidationRequest(PaymentRESTRequest paymentRESTRequest){
        this.tokenId = paymentRESTRequest.getTokenId();
        this.amount = paymentRESTRequest.getAmount();
        this.merchantId = paymentRESTRequest.getMerchantId();
        this.description = paymentRESTRequest.getDescription();
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }
}
