package com.dtu.accountService.BL;

import com.dtu.accountService.Storage.InMemoryUsersRepository;
import messaging.EventSender;
import messaging.rabbitmq.RabbitMqListener;
import messaging.rabbitmq.RabbitMqSender;

public class AccountServiceFactory {
    static AccountService accountService = null;


    public static AccountService getAccountService() {
        if (accountService != null) {
            return accountService;
        }

        EventSender eventSender = new RabbitMqSender();
        accountService = new AccountService(eventSender, new InMemoryUsersRepository());
        RabbitMqListener rabbitMqListener = new RabbitMqListener(accountService);
        try {
            rabbitMqListener.listen();
        } catch (Exception e) {
            throw new Error(e);
        }
        return accountService;
    }
}
