package com.dtu.tokenManagement.BL;

import com.dtu.tokenManagement.Storage.InMemoryTokenRepository;
import com.dtu.tokenManagement.Storage.TokenRepositoryInterface;
import messaging.EventSender;

public class TokenServiceFactory {
    static TokenService tokenService = null;

    public TokenService getTokenService(EventSender eventSender) {
        if (tokenService != null) {
            return tokenService;
        }

        TokenRepositoryInterface tokenRepository = new InMemoryTokenRepository();
        tokenService = new TokenService(tokenRepository, eventSender);

        return tokenService;
    }
}
