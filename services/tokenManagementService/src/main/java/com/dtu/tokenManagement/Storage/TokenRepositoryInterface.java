package com.dtu.tokenManagement.Storage;

import com.dtu.tokenManagement.Models.Token;

import java.util.ArrayList;

public interface TokenRepositoryInterface {
    ArrayList<Token> getTokensByUserId(String id);

    String getUserIdByTokenId(String tokenId);

    void addToken(Token token);

    void invalidateToken(Token token);

    Token getTokenById(String tokenId);
}
