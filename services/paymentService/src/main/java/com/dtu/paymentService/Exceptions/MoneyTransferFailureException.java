package com.dtu.paymentService.Exceptions;

public class MoneyTransferFailureException extends Exception {
    public MoneyTransferFailureException(String message) {
        super(message);
    }
}
