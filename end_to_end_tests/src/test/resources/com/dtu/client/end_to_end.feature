Feature: End To End Testing

Scenario: Successful payment
  Given customer 1 with name "Customer" "Test" and CPR "686798-7812" has a bank account
  And the balance of customer 1 at the bank is 1000 kr
  And customer 1 is registered with DTUPay
  And customer 1 has requested 5 tokens
  And the merchant with name "Merchant" "Test" and CPR "524174-7812" has a bank account
  And the balance of the merchant at the bank is 1000 kr
  And the merchant is registered with DTUPay
  When the merchant initiates a payment for 100 kr by the customer 1
  Then the payment 1 from customer 1 is successful
  And the balance of customer 1 at the bank will be 900 kr
  And the balance of the merchant at the bank will be 1100 kr

Scenario: Successful payments from multiple customers
  Given customer 1 with name "Customer" "Test" and CPR "423279-7812" has a bank account
  And the balance of customer 1 at the bank is 5000 kr
  And customer 1 is registered with DTUPay
  And customer 1 has requested 5 tokens
  And customer 2 with name "Customer2" "Test2" and CPR "277419-7812" has a bank account
  And the balance of customer 2 at the bank is 1000 kr
  And customer 2 is registered with DTUPay
  And customer 2 has requested 5 tokens
  And the merchant with name "Merchant" "Test" and CPR "923170-7812" has a bank account
  And the balance of the merchant at the bank is 0 kr
  And the merchant is registered with DTUPay
  When the merchant initiates a payment for 100 kr by the customer 1
  And the merchant initiates a payment for 500 kr by the customer 2
  Then the payment 1 from customer 1 is successful
  And the payment 1 from customer 2 is successful
  And the balance of customer 1 at the bank will be 4900 kr
  And the balance of customer 2 at the bank will be 500 kr
  And the balance of the merchant at the bank will be 600 kr

Scenario: Successful payments from single customer
  Given customer 1 with name "Customer" "Test" and CPR "590291-7812" has a bank account
  And the balance of customer 1 at the bank is 2000 kr
  And customer 1 is registered with DTUPay
  And customer 1 has requested 5 tokens
  And the merchant with name "Merchant" "Test" and CPR "883189-7812" has a bank account
  And the balance of the merchant at the bank is 1000 kr
  And the merchant is registered with DTUPay
  When the merchant initiates a payment for 100 kr by the customer 1
  And the merchant initiates a payment for 1000 kr by the customer 1
  Then the payment 1 from customer 1 is successful
  And the payment 2 from customer 1 is successful
  And the balance of customer 1 at the bank will be 900 kr
  And the balance of the merchant at the bank will be 2100 kr

Scenario: Failed payment for insufficient funds
  Given customer 1 with name "Customer" "Test" and CPR "642892-7812" has a bank account
  And the balance of customer 1 at the bank is 1000 kr
  And customer 1 is registered with DTUPay
  And customer 1 has requested 5 tokens
  And the merchant with name "Merchant" "Test" and CPR "426428-7812" has a bank account
  And the balance of the merchant at the bank is 1000 kr
  And the merchant is registered with DTUPay
  When the merchant initiates a payment for 5000 kr by the customer 1
  Then a "Money transfer has failed" error message is received
  And the balance of customer 1 at the bank will be 1000 kr
  And the balance of the merchant at the bank will be 1000 kr

Scenario: Failed payment for invalid token
  Given customer 1 with name "Customer" "Test" and CPR "666665-7812" has a bank account
  And the balance of customer 1 at the bank is 2000 kr
  And customer 1 is registered with DTUPay
  And customer 1 has requested 5 tokens
  And the merchant with name "Merchant" "Test" and CPR "666631-7812" has a bank account
  And the balance of the merchant at the bank is 1000 kr
  And the merchant is registered with DTUPay
  When the merchant initiates a payment for 100 kr by the customer 1
  Then the payment 1 from customer 1 is successful
  And the balance of customer 1 at the bank will be 1900 kr
  When the merchant initiates a payment for 200 kr by the customer 1
  And the customer 1 uses the same token as in payment 1
  Then a "Invalid token" error message is received

Scenario: Failed payment for invalid account
  Given customer 1 with name "Customer" "Test" and CPR "567585-7812" has a bank account
  And the balance of customer 1 at the bank is 2000 kr
  And customer 1 is registered with DTUPay
  And customer 1 has requested 5 tokens
  And the merchant with name "Merchant" "Test" and CPR "198223-7812" has a bank account
  And the balance of the merchant at the bank is 1000 kr
  When the merchant initiates a payment for 100 kr by the customer 1
  Then a "Account does not exist" error message is received

Scenario: Failed to request more than 6 tokens
  Given customer 1 with name "Customer" "Test" and CPR "453535-7812" has a bank account
  And the balance of customer 1 at the bank is 2000 kr
  And customer 1 is registered with DTUPay
  And customer 1 has requested 8 tokens
  Then a "Token request failed" error message is received

Scenario: Failed to request tokens multiple times
  Given customer 1 with name "Customer" "Test" and CPR "657567-7812" has a bank account
  And the balance of customer 1 at the bank is 2000 kr
  And customer 1 is registered with DTUPay
  And customer 1 has requested 4 tokens
  And customer 1 has requested 3 tokens
  Then a "Token request failed" error message is received

Scenario: Failed to register account when it already exists
  Given customer 1 with name "Customer" "Test" and CPR "123123-7812" has a bank account
  And customer 1 is registered with DTUPay
  When customer 2 with name "Customer" "Test" and CPR "123123-7812" has a bank account
  And customer 2 is registered with DTUPay
  Then a "User already exists" error message is received


