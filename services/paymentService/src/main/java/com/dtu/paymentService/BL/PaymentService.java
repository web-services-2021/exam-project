package com.dtu.paymentService.BL;

import com.dtu.paymentService.Exceptions.AccountValidationException;
import com.dtu.paymentService.Exceptions.MoneyTransferFailureException;
import com.dtu.paymentService.Exceptions.TokenValidationFailedException;
import com.google.gson.Gson;
import messaging.Event;
import messaging.EventReceiver;
import messaging.EventSender;
import models.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.CompletableFuture;

public class PaymentService implements EventReceiver {
    private HashMap<String, CompletableFuture<Object>> activePaymentsMap = new HashMap<>();
    private EventSender eventSender;
    ArrayList<String> bindingKeys;

    public PaymentService(EventSender eventSender) {
        this.eventSender = eventSender;
        this.bindingKeys = new ArrayList<>();
        this.bindingKeys.add("tokenValidation.response.*");
        this.bindingKeys.add("accountValidation.response.*");
        this.bindingKeys.add("transferRequest.response.*");
    }

    public void makePayment(TokenValidationRequest tokenValidationRequest) throws Exception {
        CompletableFuture<Object> result = new CompletableFuture<>();
        activePaymentsMap.put(tokenValidationRequest.getId(), result);
        validateToken(tokenValidationRequest);

        Pair<Boolean, Message> resolvedResult = (Pair<Boolean, Message>) result.join();

        if(!resolvedResult.getLeft()) {
            throw new TokenValidationFailedException("Invalid token");
        }

        result = new CompletableFuture<>();
        activePaymentsMap.put(tokenValidationRequest.getId(), result);
        validateAccount((TokenValidationResponse) resolvedResult.getRight());
        resolvedResult = (Pair<Boolean, Message>) result.join();

        if(!resolvedResult.getLeft()) {
            throw new AccountValidationException("Account does not exist");
        }

        result = new CompletableFuture<>();
        activePaymentsMap.put(tokenValidationRequest.getId(), result);

        makeTransfer((AccountValidationResponse) resolvedResult.getRight());
        resolvedResult = (Pair<Boolean, Message>) result.join();

        if(!resolvedResult.getLeft()) {
            throw new MoneyTransferFailureException("Money transfer has failed");
        }
    }

    public void validateToken(TokenValidationRequest payment) {
        Event event = new Event("tokenValidation", new Object[]{payment});
        try {
            eventSender.sendEvent("tokenValidation.request", event);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void completeTokenValidationRequest(boolean isSuccess, String paymentString) {
        TokenValidationResponse payment = new Gson().fromJson(paymentString, TokenValidationResponse.class);
        String paymentId = payment.getId();

        if(this.activePaymentsMap.containsKey(paymentId)) {
            this.activePaymentsMap.get(paymentId).complete(new Pair<>(isSuccess, payment));
        }
    }

    private void completedAccountValidationRequest(boolean isSuccess, String paymentAccount) {
        AccountValidationResponse accountValidationResponse = new Gson().fromJson(paymentAccount, AccountValidationResponse.class);
        String paymentId = accountValidationResponse.getId();

        if(this.activePaymentsMap.containsKey(paymentId)) {
            this.activePaymentsMap.get(paymentId).complete(new Pair<>(isSuccess, accountValidationResponse));
        }
    }

    private void validateAccount(TokenValidationResponse payment) {
        Event event = new Event("accountValidation", new Object[]{payment});
        try {
            eventSender.sendEvent("accountValidation.request", event);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void makeTransfer(AccountValidationResponse payment) {
        Event event = new Event("transferRequest", new Object[]{payment});
        try {
            eventSender.sendEvent("transferRequest.request", event);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void completeTransfer(boolean isSuccess, String paymentString) {
        String paymentId = new Gson().fromJson(paymentString, String.class);

        if(this.activePaymentsMap.containsKey(paymentId)) {
            this.activePaymentsMap.get(paymentId).complete(new Pair<>(isSuccess, null));
        }
    }

    @Override
    public void receiveEvent(Event event) {
        switch (event.getEventType()) {
            case "tokenValidationSuccess":
                completeTokenValidationRequest(true, (String) event.getArguments()[0]);
                break;
            case "tokenValidationFailure":
                completeTokenValidationRequest(false, (String) event.getArguments()[0]);
                break;
            case "accountValidationSuccess":
                completedAccountValidationRequest(true, (String) event.getArguments()[0]);
                break;
            case "accountValidationFailure":
                completedAccountValidationRequest(false, (String) event.getArguments()[0]);
                break;
            case "transferRequestSuccess":
                completeTransfer(true, (String) event.getArguments()[0]);
                break;
            case "transferRequestFailure":
                completeTransfer(false, (String) event.getArguments()[0]);
                break;
        }
    }

    @Override
    public ArrayList<String> getBindingKeys() throws Exception {
        if (this.bindingKeys.size() == 0) {
            throw new Exception("No binding keys specified");
        }
        return bindingKeys;
    }
}
