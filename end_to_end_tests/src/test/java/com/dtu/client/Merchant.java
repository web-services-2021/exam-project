package com.dtu.client;

import dtu.ws.fastmoney.User;

public class Merchant {
    public String dtuPayId;
    public String cpr;
    public User bankAccount;
    public String bankId;

    public Merchant(String firstName, String lastName, String cpr) {
        bankAccount = new User();
        bankAccount.setFirstName(firstName);
        bankAccount.setLastName(lastName);
        bankAccount.setCprNumber(cpr);
        this.cpr = cpr;
    }

}
