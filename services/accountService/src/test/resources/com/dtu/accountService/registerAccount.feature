Feature:
  Scenario: Create account success
    Given a cpr "123456-7890"
    And an account with cpr "123456-7891" exists
    When I register an account
    Then The account is registered

  Scenario: Create account fails because cpr already registered
    Given a cpr "123456-7890"
    And an account with cpr "123456-7890" exists
    When I register an account
    Then An exception is thrown
    And The account is not registered twice