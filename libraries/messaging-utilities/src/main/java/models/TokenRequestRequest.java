package models;

import java.util.UUID;

public class TokenRequestRequest {
    String id;
    String userId;
    int numberOfTokensRequested;

    public String getId() {
        return id;
    }

    public TokenRequestRequest(String userId, int numberOfTokensRequested) {
        this.id = UUID.randomUUID().toString();
        this.userId = userId;
        this.numberOfTokensRequested = numberOfTokensRequested;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getNumberOfTokensRequested() {
        return numberOfTokensRequested;
    }

    public void setNumberOfTokensRequested(int numberOfTokensRequested) {
        this.numberOfTokensRequested = numberOfTokensRequested;
    }
}
