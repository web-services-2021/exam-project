package models;

public class TokenValidationResponse extends Message {
    private String customerId;
    private String merchantId;


    public TokenValidationResponse(String id, String customerId, String merchantId, double amount, String description) {
        super(amount, description);
        this.customerId = customerId;
        this.merchantId = merchantId;
        this.id = id;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }
}
