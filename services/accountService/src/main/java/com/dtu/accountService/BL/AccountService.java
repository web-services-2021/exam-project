package com.dtu.accountService.BL;

import com.dtu.accountService.Exceptions.TokenRequestException;
import com.dtu.accountService.Exceptions.UserAlreadyExistsException;
import com.dtu.accountService.Exceptions.UserDoesNotExistException;
import com.dtu.accountService.Models.*;
import com.dtu.accountService.Storage.InMemoryUsersRepository;
import com.dtu.accountService.Storage.UsersRepositoryInterface;
import com.google.gson.Gson;
import messaging.Event;
import messaging.EventReceiver;
import messaging.EventSender;
import models.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.CompletableFuture;

public class AccountService implements EventReceiver {
    public HashMap<String, CompletableFuture<Object>> requestsMap = new HashMap<>();
    EventSender eventSender;
    UsersRepositoryInterface usersRepository;
    ArrayList<String> bindingKeys;

    public AccountService(EventSender eventSender, InMemoryUsersRepository usersRepository) {
        this.eventSender = eventSender;
        this.usersRepository = usersRepository;
        this.bindingKeys = new ArrayList<>();
        this.bindingKeys.add("accountValidation.request");
        this.bindingKeys.add("tokenRequest.response.*");
    }

    public ArrayList<String> fetchTokens(String userId, int numberOfTokensRequested) throws TokenRequestException {
        TokenRequestRequest request = new TokenRequestRequest(userId, numberOfTokensRequested);
        CompletableFuture<Object> result = new CompletableFuture<>();
        requestsMap.put(request.getId(), result);

        try {
            eventSender.sendEvent("tokenRequest.request", new Event("requestTokens", new Object[]{request}));
            Pair<Boolean, TokenRequestResponse> resolvedResult = (Pair<Boolean, TokenRequestResponse>) result.join();
            if (!resolvedResult.getLeft()) {
                throw new TokenRequestException("Token request failed");
            }
            return resolvedResult.getRight().getTokens();
        } catch (Exception e) {
            e.printStackTrace();
            throw new TokenRequestException("Token request failed");
        }
    }

    public String registerAccount(String cpr) throws UserAlreadyExistsException {
        User user = new User(cpr);
        usersRepository.addUser(user);
        return user.getId();
    }

    private void completeRequest(boolean isSuccess, String paymentString) {
        TokenRequestResponse response = new Gson().fromJson(paymentString, TokenRequestResponse.class);
        String requestId = response.getId();

        if (this.requestsMap.containsKey(requestId)) {
            this.requestsMap.get(requestId).complete(new Pair<>(isSuccess, response));
        }
    }

    private void validationRequest(Event event) throws Exception {
        TokenValidationResponse tokenValidationResponse = new Gson().fromJson((String) event.getArguments()[0], TokenValidationResponse.class);

        try {
            AccountValidationResponse accountValidationResponse = new AccountValidationResponse(
                    tokenValidationResponse.getId(),


                    validateAccount(tokenValidationResponse.getCustomerId()),
                    validateAccount(tokenValidationResponse.getMerchantId()),
                    tokenValidationResponse.getAmount(),
                    tokenValidationResponse.getDescription()
            );
            eventSender.sendEvent("accountValidation.response.success", new Event("accountValidationSuccess", new Object[]{
                    accountValidationResponse
            }));
        } catch (UserDoesNotExistException ignored) {
            eventSender.sendEvent("accountValidation.response.failure", new Event("accountValidationFailure", new Object[]{
                    new AccountValidationResponse(
                            tokenValidationResponse.getId(),
                            null,
                            null,
                            tokenValidationResponse.getAmount(),
                            tokenValidationResponse.getDescription()

                    )
            }));
        }
    }

    private String validateAccount(String userId) throws UserDoesNotExistException {
        return usersRepository.getCprByUserId(userId);
    }

    @Override
    public void receiveEvent(Event event) throws Exception {
        if (event.getEventType().equals("requestTokensSuccess")) {
            completeRequest(true, (String) event.getArguments()[0]);
        } else if (event.getEventType().equals("requestTokensFailure")) {
            completeRequest(false, (String) event.getArguments()[0]);
        } else if (event.getEventType().equals("accountValidation")) {
            validationRequest(event);
        }
    }

    @Override
    public ArrayList<String> getBindingKeys() throws Exception {
        if (this.bindingKeys.size() == 0) {
            throw new Exception("No binding keys specified");
        }
        return bindingKeys;
    }
}
