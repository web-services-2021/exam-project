Feature: Transfer Money

  Scenario: Successful Bank Transfer
    Given the customer "Bob" "Test" with CPR "061200-1234" has a bank account
    And the balance of the customer's account is 1000
    And the merchant "Merchant" "Merchantes" with CPR number "1234567899" has a bank account
    And the balance of the merchant's account is 2000
    When the merchant initiates a payment for 10 kr by the customer
    And I receive event "transferRequest"
    Then I have sent event "transferRequestSuccess"
    And the balance of the customer at the bank is 990 kr
    And the balance of the merchant at the bank is 2010 kr

  Scenario: The customer has no bank account
    Given a customer with CPR "999999-1111" does not have a bank account
    And the merchant "Merchant" "Merchantes" with CPR number "1234567899" has a bank account
    And the balance of the merchant's account is 2000
    When the merchant initiates a payment for 10 kr by the customer
    And I receive event "transferRequest"
    Then I have sent event "transferRequestFailure"
    And the balance of the merchant at the bank is 2000 kr

  Scenario: The merchant inserts negative amount
    Given the customer "Bob" "Test" with CPR "061200-1234" has a bank account
    And the balance of the customer's account is 1000
    And the merchant "Merchant" "Merchantes" with CPR number "1234567899" has a bank account
    And the balance of the merchant's account is 2000
    When the merchant initiates a payment for -10 kr by the customer
    And I receive event "transferRequest"
    Then I have sent event "transferRequestFailure"
    And the balance of the customer at the bank is 1000 kr
    And the balance of the merchant at the bank is 2000 kr

  Scenario: The transferred amount exceeds the balance of the customer
    Given the customer "Bob" "Test" with CPR "061200-1234" has a bank account
    And the balance of the customer's account is 1000
    And the merchant "Merchant" "Merchantes" with CPR number "1234567899" has a bank account
    And the balance of the merchant's account is 2000
    When the merchant initiates a payment for 5000 kr by the customer
    And I receive event "transferRequest"
    Then I have sent event "transferRequestFailure"
    And the balance of the customer at the bank is 1000 kr
    And the balance of the merchant at the bank is 2000 kr
