package com.dtu.client;

import dtu.ws.fastmoney.User;

import java.util.ArrayList;
import java.util.List;

public class Customer {
    public String dtuPayId;
    public String cpr;
    public List<String> tokens;
    public User bankAccount;
    public String bankId;
    public List<Boolean> paymentStatuses = new ArrayList<>();

    public Customer(String firstName, String lastName, String cpr) {
        bankAccount = new User();
        bankAccount.setFirstName(firstName);
        bankAccount.setLastName(lastName);
        bankAccount.setCprNumber(cpr);
        this.cpr = cpr;
    }

    public void addPayment(boolean payment) {
        this.paymentStatuses.add(payment);
    }
}
