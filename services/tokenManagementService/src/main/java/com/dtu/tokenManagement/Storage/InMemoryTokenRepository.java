package com.dtu.tokenManagement.Storage;

import com.dtu.tokenManagement.Models.Token;

import java.util.ArrayList;

public class InMemoryTokenRepository implements TokenRepositoryInterface {
    ArrayList<Token> tokens = new ArrayList<>();

    @Override
    public ArrayList<Token> getTokensByUserId(String id) {
        ArrayList<Token> usersTokens = new ArrayList<>();

        tokens.forEach(token -> {
            String userId = token.getUserId();
            
            if(id.equals(userId)) {
                usersTokens.add(token);
            }
        });

        return usersTokens;
    }

    @Override
    public String getUserIdByTokenId(String tokenId) {
        for(Token t : tokens) {
            String id = t.getId();

            if(tokenId.equals(id)) {
                return t.getUserId();
            }
        }

        return null;
    }

    @Override
    public void addToken(Token token) {
        tokens.add(token);
    }

    @Override
    public void invalidateToken(Token token) {
        token.setUsed(true);
    }

    @Override
    public Token getTokenById(String tokenId) {
        for(Token t : tokens) {
            String id = t.getId();

            if(tokenId.equals(id)) {
                return t;
            }
        }

        return null;
    }
}
