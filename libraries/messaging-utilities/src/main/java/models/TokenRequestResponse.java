package models;

import java.util.ArrayList;

public class TokenRequestResponse {
    String id;
    String userId;
    ArrayList<String> tokens;

    public TokenRequestResponse(String id, String userId, ArrayList<String> tokens) {
        this.id = id;
        this.userId = userId;
        this.tokens = tokens;
    }

    public String getId() {
        return id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public ArrayList<String> getTokens() {
        return tokens;
    }

    public void setTokens(ArrayList<String> tokens) {
        this.tokens = tokens;
    }
}
