package com.dtu.accountService.Storage;

import com.dtu.accountService.Models.User;
import com.dtu.accountService.Exceptions.UserAlreadyExistsException;
import com.dtu.accountService.Exceptions.UserDoesNotExistException;

import java.util.ArrayList;

public class InMemoryUsersRepository implements UsersRepositoryInterface {
    public ArrayList<User> users = new ArrayList<>();

    @Override
    public String getCprByUserId(String userId) throws UserDoesNotExistException {
        for (User u : users) {
            if (u.getId().equals(userId)) {
                return u.getCpr();
            }
        }
        throw new UserDoesNotExistException("User does not exist");
    }

    @Override
    public void addUser(User user) throws UserAlreadyExistsException {
        for (User u : users) {
            if (u.getCpr().equals(user.getCpr())) {
                throw new UserAlreadyExistsException("User already exists");
            }
        }
        users.add(user);
    }
}
