package com.dtu.accountService.REST;

import com.dtu.accountService.BL.AccountService;
import com.dtu.accountService.BL.AccountServiceFactory;
import com.dtu.accountService.Exceptions.TokenRequestException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/tokens")
public class TokensResource {

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response generateTokens(@FormParam("userId") String userId, @FormParam("requestedTokens") int requestedTokens) {
        try{
            AccountService service = AccountServiceFactory.getAccountService();
            return Response.status(201).entity(service.fetchTokens(userId,requestedTokens)).build();
        } catch (TokenRequestException e) {
            return Response.status(400).entity(e.getMessage()).build();
        }
    }
}
