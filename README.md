[![Build Status](http://g-08.compute.dtu.dk:8282/buildStatus/icon?job=exam-project)](http://g-08.compute.dtu.dk:8282/job/exam-project/)

# DTUPay - Group 8

This is the final project for course 02667 at DTU.

### Overview

The project consists of several microservices that make
use of REST and SOAP API to communicate externally, and rabbitMq (message broker) to
communicate internally.

There are end-to-end tests to test all the services together along with tests for
each individual service to make sure everything is working as intended. Cucumber 
feature tests are used for all testing.

### Microservices

The project consists of the following microservices:

- The payment microservice in `services/paymentService` which handles payment requests
  from a merchant.
- The token management microservice in `services/tokenManagementService` which handles
  the validation of tokens that are issued along with a payment request.
- The money transfer microservice in `services/moneyTransferService` which handles the
  SOAP API communication with the external bank structure.
- The account management microservice `services/accountManagementService` which handles
  the customers' and merchants' accounts on DTUPay and let them create an account.

### Project build

The main `docker-compose.yml` file is found in `end_to_end_tests`.

The project is build using `build_and_run.sh`.